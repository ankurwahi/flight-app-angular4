import { Component, OnInit,EventEmitter,Output } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import {FlightDTO } from '../shared/flight.dto';
import {CityDTO } from '../shared/city.dto';
import { FlightService } from '../shared/flight.service';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';

@Component({
  selector: 'utu-flightsearch-form',
  templateUrl: './flightsearch-form.component.html',
  styleUrls: ['./flightsearch-form.component.css']
})


export class FlightsearchFormComponent implements OnInit {

  private city$: Observable<CityDTO[]>;
  public filteredFrom: Observable<CityDTO[]>;
  public filteredTo: Observable<CityDTO[]>;
  public cityArray:any;

  public myForm: FormGroup;
  public submitted: boolean;
  public events: any[] = [];




  constructor(private _fb: FormBuilder,
              private FlightService: FlightService
  ) {
  }

  filterStates(name: string) {
    return this.cityArray.filter(state =>
    state.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }


  @Output() search:EventEmitter<FlightDTO> = new EventEmitter();

  ngOnInit() {


    this.myForm = this._fb.group({
      from: [new FormControl(),[<any>Validators.required]],
      to: [new FormControl(),[<any>Validators.required]],
      departure: [new Date(), [<any>Validators.required]],
      return: [new Date(), [<any>Validators.required]],
      passengers: [new FormControl(), [<any>Validators.required]],
      maximum:new FormControl(),
      minimum:new FormControl()
    });

    //get all cities from json
    this.city$ = this.FlightService.getCities();

    //put cities in dropdown
    this.city$.subscribe(data => {
      this.cityArray=data;
      this.filteredFrom = this.myForm.get('from').valueChanges
        .startWith(null)
        .map(state => state ? this.filterStates(state) : data.slice());

      this.filteredTo = this.myForm.get('to').valueChanges
        .startWith(null)
        .map(state => state ? this.filterStates(state) : data.slice());
    })


    // subscribe to form changes
    this.subcribeToFormChanges();

    const people = {
      from:'',
      to:'',
      departure:new Date(),
      return:new Date(),
      minimum:1000,
      maximum:50000,
      passengers:'2',
    };

    (<FormGroup>this.myForm)
      .setValue(people, { onlySelf: true });
  }

  /**
   * on save
   * @param e
   * @param model
   * @param isValid
   */
  save(e,model: FlightDTO, isValid: boolean) {
    e.preventDefault();
    this.submitted = true;
    console.log(model, isValid);
    this.search.emit(model);
  }

  subcribeToFormChanges() {
    const myFormStatusChanges$ = this.myForm.statusChanges;
    const myFormValueChanges$ = this.myForm.valueChanges;
    myFormStatusChanges$.subscribe(x => this.events.push({ event: 'STATUS_CHANGED', object: x }));
    myFormValueChanges$.subscribe(x => this.events.push({ event: 'VALUE_CHANGED', object: x }));
  }



}
