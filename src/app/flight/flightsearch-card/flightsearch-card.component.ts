import { Component, OnInit,Input } from '@angular/core';
import {FlightDTO } from '../shared/flight.dto';
@Component({
  selector: 'utu-flightsearch-card',
  templateUrl: './flightsearch-card.component.html',
  styleUrls: ['./flightsearch-card.component.css']
})
export class FlightsearchCardComponent implements OnInit {
  constructor() {
    }
  @Input() public flights:FlightDTO[] = [];
  @Input() name: string;
  ngOnInit() {
  }
}
