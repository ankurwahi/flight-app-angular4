export interface FlightDTO {
  id:string,
  name:string,
  from:string,
  to:string,
  departure:string,
  returnDeparture:string,
  cost:string,
  minimum:string,
  maximum:string,
  img:string
}
