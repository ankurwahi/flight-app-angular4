import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';


const routes:Routes = [

  {path: '', redirectTo: '/flight/search', pathMatch: 'full'},
  {path: 'flight', loadChildren: './flight/flight.module#FlightModule'},

]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
