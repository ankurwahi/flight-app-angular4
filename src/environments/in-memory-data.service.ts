﻿ import { InMemoryDbService } from 'angular-in-memory-web-api';
 import { Injectable } from '@angular/core'
 import { FlightDTO } from '../app/flight/shared/flight.dto';
 import { CityDTO } from '../app/flight/shared/city.dto';


 @Injectable()
 export class InMemoryDataService implements InMemoryDbService {
   public createDb() {


     const cities =
       [

         {id: '1', name:'Delhi',flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'},
         {id: '2', name:'Kolkata',flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'},
         {id: '3', name:'Mumbai',flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'},
         {id: '4', name:'Chandigarh',flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'},
         {id: '5', name:'Bhubneshwar',flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'},
         {id: '6', name:'Ahmedabad',flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'},
         {id: '7', name:'Chennai',flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'},
         {id: '8', name:'Banglore',flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'},
         {id: '9', name:'Hyderabad',flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'},
         {id: '10', name:'Amritsar',flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'},
       ]
     const flights =
       [

         {id: 'K201', name:'Kingfisher', from: 'Delhi', to: 'Kolkata', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-15T07:16:40.2996298+00:00', cost:'5000',img:'https://pbs.twimg.com/profile_images/2203484703/Profile-pic-2_400x400.jpg',available:10},
         {id: 'AI203', name:'Airindia', from: 'Delhi', to: 'Kolkata', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-16T07:16:40.2996298+00:00', cost:'7000',img:'http://www.travelandtourworld.com/wp-content/uploads/2013/08/Air-India.jpg',available:20},
         {id: 'SJ101', name:'Spicejet', from: 'Delhi', to: 'Kolkata', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-17T07:16:40.2996298+00:00', cost:'8000',img:'http://ndl.mgccw.com/mu3/app/20141013/16/1413197490509/icon/icon_xxl.png',available:12},
         {id: 'IND201', name:'Indigo', from: 'Delhi', to: 'Kolkata', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-18T07:16:40.2996298+00:00', cost:'12000',img:'https://thescottbrothers.com/wp-content/uploads/2016/03/indigo.gif',available:50},
         {id: 'K211', name:'Kingfisher', from: 'Delhi', to: 'Mumbai', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-15T07:16:40.2996298+00:00', cost:'5000',img:'https://pbs.twimg.com/profile_images/2203484703/Profile-pic-2_400x400.jpg',available:14},
         {id: 'AI213', name:'Airindia', from: 'Delhi', to: 'Mumbai', departure:'2017-11-17T07:16:40.2996298+00:00',returnDeparture:'2017-11-16T07:16:40.2996298+00:00', cost:'7000',img:'http://www.travelandtourworld.com/wp-content/uploads/2013/08/Air-India.jpg',available:13},
         {id: 'AI213', name:'Airindia', from: 'Delhi', to: 'Kolkata', departure:'2017-11-20T07:16:40.2996298+00:00',returnDeparture:'2017-11-16T07:16:40.2996298+00:00', cost:'7000',img:'http://www.travelandtourworld.com/wp-content/uploads/2013/08/Air-India.jpg',available:13},
         {id: 'AI213', name:'Airindia', from: 'Delhi', to: 'Kolkata', departure:'2017-11-21T07:16:40.2996298+00:00',returnDeparture:'2017-11-16T07:16:40.2996298+00:00', cost:'7000',img:'http://www.travelandtourworld.com/wp-content/uploads/2013/08/Air-India.jpg',available:13},
         {id: 'AI213', name:'Airindia', from: 'Delhi', to: 'Kolkata', departure:'2017-11-22T07:16:40.2996298+00:00',returnDeparture:'2017-11-16T07:16:40.2996298+00:00', cost:'7000',img:'http://www.travelandtourworld.com/wp-content/uploads/2013/08/Air-India.jpg',available:13},
         {id: 'K201', name:'Kingfisher', from: 'Delhi', to: 'Kolkata', departure:'2017-11-20T07:16:40.2996298+00:00',returnDeparture:'2017-11-15T07:16:40.2996298+00:00', cost:'5000',img:'https://pbs.twimg.com/profile_images/2203484703/Profile-pic-2_400x400.jpg',available:10},
         {id: 'K201', name:'Kingfisher', from: 'Delhi', to: 'Kolkata', departure:'2017-11-21T07:16:40.2996298+00:00',returnDeparture:'2017-11-15T07:16:40.2996298+00:00', cost:'5000',img:'https://pbs.twimg.com/profile_images/2203484703/Profile-pic-2_400x400.jpg',available:10},
         {id: 'K201', name:'Kingfisher', from: 'Delhi', to: 'Kolkata', departure:'2017-11-22T07:16:40.2996298+00:00',returnDeparture:'2017-11-15T07:16:40.2996298+00:00', cost:'5000',img:'https://pbs.twimg.com/profile_images/2203484703/Profile-pic-2_400x400.jpg',available:10},


       ]


     return {
       flights,
       cities
     };
   }

   public responseInterceptor(responseOptions:any, requestInfo:any):any {
     if (responseOptions.body) {
       let responseBody = responseOptions.body.data;
       responseOptions.body = responseBody;
     }
     return responseOptions;
   }
 }
