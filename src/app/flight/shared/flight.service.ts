import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers, Response } from '@angular/http'
import { Observable } from 'rxjs/Observable';
import { FlightDTO } from './flight.dto';
import { CityDTO } from './city.dto';
import { FLIGHT_URL,CITY_URL } from '../../app.endpoint';
import * as moment from 'moment';


@Injectable()
export class FlightService {

  private headers = new Headers({ 'Content-Type': 'application/json' });
  private URL_FLIGHT: string = FLIGHT_URL;
  private URL_CITY: string = CITY_URL;

  constructor(private _http: Http ) {
  }

  public getAll(flightDTO:FlightDTO): Observable<any> {
    return this._http.get(this.URL_FLIGHT).map((res: Response) =>
    {
      var response= res.json();
      return response.filter(data =>
      data.from === flightDTO.from &&
      data.to===flightDTO.to &&
      moment(data.departure).format('DD')==moment(flightDTO.departure).format('DD') &&
      flightDTO.minimum<=data.cost && flightDTO.maximum>=data.cost
      )
    })

  }

  public getCities(): Observable<any> {
    return this._http.get(this.URL_CITY).map((res: Response) =>
    {
      var response= res.json();
      return response as CityDTO
    });

  }



}
