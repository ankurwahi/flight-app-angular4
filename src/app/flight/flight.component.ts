import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import {FlightDTO } from './shared/flight.dto';
import { FlightService } from './shared/flight.service';
@Component({
  selector: 'utu-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {
  isSearching:Boolean=false;
  constructor(private flightService:FlightService) {
  }
  private flights$:Observable<FlightDTO>;
  ngOnInit() {
  }
  searchHandler(flightDTO:FlightDTO) {
    this.isSearching=true;
    console.log(flightDTO);
    this.flights$ = this.flightService.getAll(flightDTO);
    this.flights$.subscribe(data => {
      this.isSearching=false;
     });
  }
}
